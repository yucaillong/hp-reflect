package com.kvn.reflect;

import org.junit.Test;

import java.lang.reflect.Method;
import java.util.concurrent.TimeUnit;

/**
 * Created by wangzhiyuan on 2018/8/13
 */
public class MethodAccessTest {

    @Test
    public void get() throws Exception {
        for (int j = 0; j < 100; j++) {
            FooService fooService = new FooService();

            long t0 = System.currentTimeMillis();
            int times = 100000;
            for (int i = 0; i < times; i++) {
                executePrivateByHp(fooService);
            }
            long t1 = System.currentTimeMillis();

            for (int i = 0; i < times; i++) {
                executePrivateByReflect(fooService);
            }
            long t2 = System.currentTimeMillis();

            for (int i = 0; i < times; i++) {
                executePublicByHp(fooService);
            }
            long t3 = System.currentTimeMillis();

            for (int i = 0; i < times; i++) {
                executePublicByReflect(fooService);
            }
            long t4 = System.currentTimeMillis();

            System.out.println("executePrivateByHp，循环" + times + "次，耗时【" + (t1 - t0) + "ms】");
            System.out.println("executePrivateByReflect，循环" + times + "次，耗时【" + (t2 - t1) + "ms】");
            System.out.println("executePublicByHp，循环" + times + "次，耗时【" + (t3 - t2) + "ms】");
            System.out.println("executePublicByReflect，循环" + times + "次，耗时【" + (t4 - t3) + "ms】");
            System.out.println("---------------------------------------------------\n");
            TimeUnit.SECONDS.sleep(2);
        }


    }

    private void executePublicByReflect(FooService fooService) {
        try {
            fooService = new FooService();
            Method method2 = fooService.getClass().getDeclaredMethod("method22", String[].class);
            Object rlt = method2.invoke(fooService, new Object[]{new String[]{"xxx"}});
//            System.out.println("rlt:" + rlt);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void executePublicByHp(FooService fooService) {
        MethodAccess methodAccess = MethodAccess.get(FooService.class);
        Object rlt = methodAccess.invoke(fooService, "method22", new Class[]{String[].class}, new Object[]{new String[]{"xxx"}});
//        System.out.println("rlt:" + rlt);
    }

    private void executePrivateByReflect(FooService fooService) {
        try {
            Method method1 = fooService.getClass().getDeclaredMethod("method11", String.class);
            method1.setAccessible(true);
            Object rlt = method1.invoke(fooService, "xxx");
//            System.out.println("rlt:" + rlt);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void executePrivateByHp(FooService fooService) {
        MethodAccess methodAccess = MethodAccess.get(FooService.class);
        Object rlt = methodAccess.invoke(fooService, "method11", "xxx");
//        System.out.println("rlt:" + rlt);
    }
}