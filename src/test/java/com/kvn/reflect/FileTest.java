package com.kvn.reflect;

import com.kvn.reflect.compiler.JavassistCompiler;

import java.nio.file.Files;
import java.nio.file.Paths;

/**
 * Created by wangzhiyuan on 2018/8/14
 */
public class FileTest {
    public static void main(String[] args) throws Exception {
        String path = "E:\\ws000\\hp-reflect\\src\\test\\java\\com\\kvn\\reflect\\FooServiceFieldAccess.java.bak";
        /** 获取源代码，并将所有的\r都移除。即换行统一使用\n。 JavassistCompiler 的实现中只支持\n的换行。*/
        String code = new String(Files.readAllBytes(Paths.get(path))).replaceAll("\r", "");
        Class<?> clazz = JavassistCompiler.instance().compile(code, AccessClassLoader.instance());
        Object newInstance = clazz.newInstance();
        System.out.println("newInstance = [" + newInstance + "]");
    }
}
